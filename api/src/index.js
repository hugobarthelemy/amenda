const app = require('express')();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
 * @api {get} /api/v1/ping Ping
 * @apiVersion 1.0.0
 * @apiDescription Test if the API server is running/responding.
 * @apiName Ping
 * @apiGroup Utils
 *
 * @apiSuccess {String} ping pong
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {"ping":"pong"}
 */
app.use('/api/v1/ping', require('./ping'));

app.listen(3001, () => {
  console.log('API server listening on port 3001')
});
