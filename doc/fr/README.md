---
description: Documentation du projet Amenda, un outil de simplification du dépôt des amendements pour les députés et les citoyens.
---

# Introduction

Amenda est un outil de simplification du dépôt des amendements pour les députés en vue d'une meilleure visibilité et
d'un meilleur échange avec les citoyens.
